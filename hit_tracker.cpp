#include <TMath.h>
#include <TGraph2D.h>
#include <TRandom2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TF2.h>
#include <TH1.h>
#include <Math/Functor.h>
#include <TPolyLine3D.h>
#include <Math/Vector3D.h>
#include <Fit/Fitter.h>
#include <TApplication.h>
#include <TPaveLabel.h>
#include <iostream>
#include <fstream>
#include <typeinfo>
#include <cctype>
#include <string>
#include <algorithm>
#include <cassert>
#include <TF1.h>
#include <TGraphErrors.h>
#include <TRandom.h>
#include <TLegend.h>
#include <TMinuit.h>
#include <vector>
#include <TAxis.h>
#include <stdio.h>
#include <TF3.h>
#include <math.h>
#include <stdio.h>
#include <TText.h>
#include <TLatex.h>
#include <cmath>
#include <TMarker.h>


//this main is for hit_tracker_w_time project
//this project is compiled with VS 2010


using namespace std;


using namespace ROOT::Math;



ofstream points_xi("ppxi.txt");
ofstream points_yi("ppyi.txt");

ofstream pointsx("ppx.txt");
ofstream pointsy("ppy.txt");


ofstream theta("delta_t.txt");

static vector<double> x ,y , xz, yz, xsig, ysig, xzsig, yzsig, time_infx, time_infy; // vectors to hold the original information coming from the data file
static vector<int> sign_side_x, sign_side_y;

static vector<double> x_t ,y_t , xz_t, yz_t, xsig_t, ysig_t, xzsig_t, yzsig_t; // vectors to hold the hold selected values after time-filtration


static const double r_indx = 1.58; //refractive index used in time correction
static const double fa = 0.001; // used in the calculation of maximum log

static vector<double> TXtot, TYtot; //vectors to hold all the time data for the histogram at the end

static vector<double> thetavalues; //vector to hold the opening angles

static double p1,p2,p3,p4; // the four parameters of the line equation
static double xm, ym, zxm, zym ; // maximum x,y and z values
static double o_xpos, o_ypos, o_zpos , o_xsp, o_ysp, o_zsp; //real values of the muon used to find the opening angles

void find_para_or(){ //this function uses the real values of the muon to calculate the line parameters
    double su_x, su_zx ,su_zx2, su_zxx , su_y, su_zy, su_zy2, su_zyy;

    su_x = o_xpos + (o_xpos+(0.1*o_xsp));
    su_zx = o_zpos + (o_zpos+(0.1*o_zsp));
    su_zx2 = pow(o_zpos,2) + pow((o_zpos+(0.1*o_zsp)),2);
    su_zxx = (o_zpos*o_xpos) + ((o_xpos+(0.1*o_xsp))*(o_zpos+(0.1*o_zsp)));
    su_y = o_ypos + (o_ypos+(0.1*o_ysp));
    su_zy = su_zx;
    su_zy2 = su_zx2;
    su_zyy = (o_zpos*o_ypos) + ((o_ypos+(0.1*o_ysp))*(o_zpos+(0.1*o_zsp)));

    p1 = ((su_x*su_zx2) - (su_zx*su_zxx))/((double(2)*su_zx2)-(pow(su_zx,2)));
    p2 = ((2*su_zxx)-(su_zx*su_x))/((double(2)*su_zx2)-(pow(su_zx,2)));

    p3 = ((su_y*su_zy2) - (su_zy*su_zyy))/((double(2)*su_zy2)-(pow(su_zy,2)));
    p4 = ((2*su_zyy)-(su_zy*su_y))/((double(2)*su_zy2)-(pow(su_zy,2)));

}

void calc_para() //this function used the vectors after time-filtration to calculate the parameters
{
    double su_x, su_zx ,su_zx2, su_zxx , su_y, su_zy, su_zy2, su_zyy;
    su_x =0;
    su_zx =0;
    su_zx2 =0;
    su_zxx =0;
    su_y =0;
    su_zy =0;
    su_zy2 =0;
    su_zyy =0;
    for(int i=0;i<x_t.size();i++){
        su_x += x_t[i];
        su_zx += xz_t[i];
        su_zx2 += pow(xz_t[i],2);
        su_zxx += x_t[i]*xz_t[i];
    }

    for(int i=0;i<y_t.size();i++){
        su_y += y_t[i];
        su_zy += yz_t[i];
        su_zy2 += pow(yz_t[i],2);
        su_zyy += y_t[i]*yz_t[i];
    }

    p1 = ((su_x*su_zx2) - (su_zx*su_zxx))/((double(x_t.size())*su_zx2)-(pow(su_zx,2)));
    p2 = ((x_t.size()*su_zxx)-(su_zx*su_x))/((double(x_t.size())*su_zx2)-(pow(su_zx,2)));

    p3 = ((su_y*su_zy2) - (su_zy*su_zyy))/((double(y_t.size())*su_zy2)-(pow(su_zy,2)));
    p4 = ((y_t.size()*su_zyy)-(su_zy*su_y))/((double(y_t.size())*su_zy2)-(pow(su_zy,2)));
}

void calc_para_o() //this function uses the direct values with no time filters to calculate the parameters
{
    double su_x, su_zx ,su_zx2, su_zxx , su_y, su_zy, su_zy2, su_zyy;
    su_x =0;
    su_zx =0;
    su_zx2 =0;
    su_zxx =0;
    su_y =0;
    su_zy =0;
    su_zy2 =0;
    su_zyy =0;
    for(int i=0;i<x.size();i++){
        su_x += x[i];
        su_zx += xz[i];
        su_zx2 += pow(xz[i],2);
        su_zxx += x[i]*xz[i];
    }

    for(int i=0;i<y.size();i++){
        su_y += y[i];
        su_zy += yz[i];
        su_zy2 += pow(yz[i],2);
        su_zyy += y[i]*yz[i];
    }

    p1 = ((su_x*su_zx2) - (su_zx*su_zxx))/((double(x.size())*su_zx2)-(pow(su_zx,2)));
    p2 = ((x.size()*su_zxx)-(su_zx*su_x))/((double(x.size())*su_zx2)-(pow(su_zx,2)));

    p3 = ((su_y*su_zy2) - (su_zy*su_zyy))/((double(y.size())*su_zy2)-(pow(su_zy,2)));
    p4 = ((y.size()*su_zyy)-(su_zy*su_y))/((double(y.size())*su_zy2)-(pow(su_zy,2)));
}





//simple line function
double myfun(double var,  double* par){
    return par[0]+(var*par[1]);
}


double tofx(int pos){ //time of flight in the x-direction

    double d = sqrt(pow(x[pos]-xm,2)+pow(xz[pos]-zxm,2));
    return 0.003335 * d ;
}


double tofy(int pos){ //time of flight in the y-direction

    double d = sqrt(pow(y[pos]-ym,2)+pow(yz[pos]-zym,2));
    return 0.003335 * d ;
}

//the function used in minuit minimizer
void fcn2(Int_t &npar, Double_t *gin, Double_t &f, Double_t *par, Int_t iflag)
{
   const Int_t nbins = x_t.size();
   Int_t i;

   Double_t chisq = 0;
   for (i=0;i<nbins; i++) {
     if( (myfun(xz_t[i],par)) >= (x_t[i]-xsig_t[i]) && (myfun(xz_t[i],par)) <= (x_t[i]+xsig_t[i]) ){chisq -= log(1.0);}
     else {chisq -= log(fa);}
   }
   f = chisq;
}

//second function of minuit
void fcn3(Int_t &npar, Double_t *gin, Double_t &f, Double_t *par, Int_t iflag)
{
   const Int_t nbins = y_t.size();
   Int_t i;

   Double_t chisq = 0;
   for (i=0;i<nbins; i++) {
     if( (myfun(yz_t[i],par)) >= (y_t[i]-ysig_t[i]) && (myfun(yz_t[i],par)) <= (y_t[i]+ysig_t[i]) ){chisq -= log(1.0);}
     else {chisq -= log(fa);}
   }
   f = chisq;
}


//the fitting function
void fitting(){



    ifstream f1("Triggered_Mu_single.txt"); //the data file
    int NN = 1; //track the number of muons
    ofstream res("out.txt"); // output file
    double id,side,xmin,xmax,xc,xs,ymin,ymax,yc,ys,zmin,zmax,zc,zs,charge,h_time;
    string ins;
    string acc,ign;
    acc = "Ch"; //to know which line from which
    ign = "Muon";
    f1>>ins;
    res << "Muon number,     value of : p1       p2      p3      p4         entry point: x        y       z" << endl;
    while(!f1.eof()){
        if (ins==ign){
            x.clear();
            y.clear();
            xz.clear();
            yz.clear();
            xsig.clear();
            ysig.clear();
            xzsig.clear();
            yzsig.clear();
            time_infx.clear();
            time_infy.clear();
            sign_side_x.clear();
            sign_side_y.clear();


            //taking the original values
            f1 >> o_xpos;
            f1 >> o_ypos;
            f1 >> o_zpos;
            f1 >> o_xsp;
            f1 >> o_ysp;
            f1 >> o_zsp;

            while(!f1.eof()){
                f1>>ins;
                if (ins==acc){;}
                else if (ins==ign){break;}
                else {
                    id = stod(ins);
                    f1>>side;
                    f1>>xmin;
                    f1>>xmax;
                    f1>>ymin;
                    f1>>ymax;
                    f1>>zmin;
                    f1>>zmax;
                    f1>>charge;
                    f1>>h_time;
                    xc = (xmin+xmax)*0.5;
                    yc = (ymin+ymax)*0.5;
                    zc = (zmin+zmax)*0.5;
                    xs = (xc - xmin);
                    ys = (yc - ymin);
                    zs = (zc - zmin);
                    //filling different vectors depending if sigma is too high
                    if(xs<1000){
                        x.push_back(xc);
                        xz.push_back(zc);
                        xsig.push_back(xs);
                        xzsig.push_back(zs);
                        time_infx.push_back(h_time);
                        sign_side_x.push_back(side);
                    }
                    if(ys<1000){
                        y.push_back(yc);
                        yz.push_back(zc);
                        ysig.push_back(ys);
                        yzsig.push_back(zs);
                        time_infy.push_back(h_time);
                        sign_side_y.push_back(side);
                    }
                }

            }

        }

        if(!x.empty()){
            points_xi << NN << endl;
            points_yi << NN << endl;



            double xpos,ypos,zpos,erp1,erp2,erp3,erp4;


            calc_para_o();

            //find_para_or();

            x_t.clear();
            xz_t.clear();
            xsig_t.clear();
            xzsig_t.clear();
            y_t.clear();
            yz_t.clear();
            yzsig_t.clear();
            ysig_t.clear();

            double xzma = *max_element(xz.begin(),xz.end());
            double xma = (xzma * p2 ) + p1;

            vector<double> diff_side0;
            vector<double> diff_side1;

            // finding the zero time-position for x and y for each side

            for (int i=0; i<x.size();i++){
                if(xzma == xz[i]){
                    if(sign_side_x[i] == 0){
                        diff_side0.push_back(abs(x[i]-xma));
                        diff_side1.push_back(100000);
                    }
                    if(sign_side_x[i] == 1){
                        diff_side1.push_back(abs(x[i]-xma));
                        diff_side0.push_back(100000);
                    }
                }
                else{
                    diff_side0.push_back(100000);
                    diff_side1.push_back(100000);
                }
            }




            int mininx_side0 = min_element(diff_side0.begin(),diff_side0.end())-diff_side0.begin();
            int mininx_side1 = min_element(diff_side1.begin(),diff_side1.end())-diff_side1.begin();

            zxm = xzma;
            if (x[mininx_side0] == x[mininx_side1]){xm = x[mininx_side0];}
            else if(abs(x[mininx_side0]-xma)<abs(x[mininx_side1]-xma)){xm = x[mininx_side0];}
            else if(abs(x[mininx_side0]-xma)>abs(x[mininx_side1]-xma)){xm = x[mininx_side1];}

            double yzma = *max_element(yz.begin(),yz.end());
            double yma = (yzma * p4 ) + p3;

            diff_side0.clear();
            diff_side1.clear();

            for (int i=0; i<y.size();i++){
                if(yzma == yz[i]){
                    if(sign_side_y[i] == 0){
                        diff_side0.push_back(abs(y[i]-yma));
                        diff_side1.push_back(100000);
                    }
                    if(sign_side_y[i] == 1){
                        diff_side1.push_back(abs(y[i]-yma));
                        diff_side0.push_back(100000);
                    }
                }
                else{
                    diff_side0.push_back(100000);
                    diff_side1.push_back(100000);
                }
            }

            int mininy_side0 = min_element(diff_side0.begin(),diff_side0.end())-diff_side0.begin();
            int mininy_side1 = min_element(diff_side1.begin(),diff_side1.end())-diff_side1.begin();

            zym = yzma;
            if (y[mininy_side0] == y[mininy_side1]){ym = y[mininy_side0];}
            else if(abs(y[mininy_side0]-yma)<abs(y[mininy_side1]-yma)){ym = y[mininy_side0];}
            else if(abs(y[mininy_side0]-yma)>abs(y[mininy_side1]-yma)){ym = y[mininy_side1];}




            //finding the corrected time

            vector<double> t_v_new;

            for (int i = 0; i < x.size(); i++){
                double tmu = tofx(i); //the time of flight
                double ti;
                double t_new;
                if (sign_side_x[i] == 0){ //change in x
                    ti = (x[i]-x[mininx_side0]) * 0.00335 * r_indx;
                    t_new = time_infx[i] - time_infx[mininx_side0] - tmu - ti;
                }
                if (sign_side_x[i] == 1){
                    ti = (x[mininx_side1]-x[i]) * 0.00335 * r_indx;
                    t_new = time_infx[i] - time_infx[mininx_side1] - tmu - ti;
                }
                t_v_new.push_back(t_new);
                if(i!=mininx_side0 && i!=mininx_side1 ){TXtot.push_back(t_new);}
            }

            for (int i = 0; i < x.size(); i++){ //discarding points that lay beyond -+ 30 corrected time
                if(abs(t_v_new[i])<30 ){
                    if(zxm != xz[i]){
                        x_t.push_back(x[i]);
                        xsig_t.push_back(xsig[i]);
                        xz_t.push_back(xz[i]);
                        xzsig_t.push_back(xzsig[i]);
                    }
                }

            }


            x_t.push_back(x[mininx_side0]);
            xsig_t.push_back(xsig[mininx_side0]);
            xz_t.push_back(xz[mininx_side0]);
            xzsig_t.push_back(xzsig[mininx_side0]);
            x_t.push_back(x[mininx_side1]);
            xsig_t.push_back(xsig[mininx_side1]);
            xz_t.push_back(xz[mininx_side1]);
            xzsig_t.push_back(xzsig[mininx_side1]);




            check: //a second filter choosing one point if there are multiple having the same z coordinates
                for(int i=0;i<x_t.size();i++){
                    for (int j=0;j<x_t.size();j++){
                        if (i!=j){
                            if (xz_t[i] == xz_t[j]){
                                if (t_v_new[i]<t_v_new[j]){
                                    x_t.erase(x_t.begin()+j);
                                    xz_t.erase(xz_t.begin()+j);
                                    xsig_t.erase(xsig_t.begin()+j);
                                    xzsig_t.erase(xzsig_t.begin()+j);
                                    goto check;
                                }
                                if (t_v_new[j]<t_v_new[i]){
                                    x_t.erase(x_t.begin()+i);
                                    xz_t.erase(xz_t.begin()+i);
                                    xsig_t.erase(xsig_t.begin()+i);
                                    xzsig_t.erase(xzsig_t.begin()+i);
                                    goto check;
                                }
                            }
                        }
                    }

                }





            for (int i =0;i<x.size();i++){
                points_xi << x[i] << " " << xz[i] << " " << time_infx[i] << " " << t_v_new[i] << endl;

            }



            t_v_new.clear();


            //same process for y-direction
            for (int i = 0; i < y.size(); i++){
                double tmu = tofy(i);
                double ti;
                double t_new;
                if (sign_side_y[i] == 0){
                    ti = (y[i]-y[mininy_side0]) * 0.00335 * r_indx;
                    t_new = time_infy[i] - time_infy[mininy_side0] - tmu - ti;
                }
                if (sign_side_y[i] == 1){
                    ti = (y[mininy_side1]-y[i]) * 0.00335 * r_indx;
                    t_new = time_infy[i] - time_infy[mininy_side1] - tmu - ti;
                }

                t_v_new.push_back(t_new);
                if(i!=mininy_side0 && i!=mininy_side1){TYtot.push_back(t_new);}
            }


            for (int i = 0; i < y.size(); i++){
                if (abs(t_v_new[i])<30){
                    if(zym != yz[i]){
                        y_t.push_back(y[i]);
                        ysig_t.push_back(ysig[i]);
                        yz_t.push_back(yz[i]);
                        yzsig_t.push_back(yzsig[i]);
                    }
                }
            }

            y_t.push_back(y[mininy_side0]);
            ysig_t.push_back(ysig[mininy_side0]);
            yz_t.push_back(yz[mininy_side0]);
            yzsig_t.push_back(yzsig[mininy_side0]);
            y_t.push_back(y[mininy_side1]);
            ysig_t.push_back(ysig[mininy_side1]);
            yz_t.push_back(yz[mininy_side1]);
            yzsig_t.push_back(yzsig[mininy_side1]);


            check2:
                for(int i=0;i<y_t.size();i++){
                    for (int j=0;j<y_t.size();j++){
                        if (i!=j){
                            if (yz_t[i] == yz_t[j]){
                                if (t_v_new[i]<t_v_new[j]){
                                    y_t.erase(y_t.begin()+j);
                                    yz_t.erase(yz_t.begin()+j);
                                    ysig_t.erase(ysig_t.begin()+j);
                                    yzsig_t.erase(yzsig_t.begin()+j);
                                    goto check2;
                                }
                                if (t_v_new[j]<t_v_new[i]){
                                    y_t.erase(y_t.begin()+i);
                                    yz_t.erase(yz_t.begin()+i);
                                    ysig_t.erase(ysig_t.begin()+i);
                                    yzsig_t.erase(yzsig_t.begin()+i);
                                    goto check2;
                                }
                            }
                        }
                    }

                }


            for (int i =0;i<y.size();i++){
                points_yi << y[i] << " " << yz[i] << " " << time_infy[i] << " " << t_v_new[i] << endl;

            }


            points_xi << endl;
            points_yi << endl;
            points_xi << endl;
            points_yi << endl;






            calc_para();


            if(p1 != p1 || p2 != p2 || p3 != p3 || p4 != p4){  //fail safe if any parameter is infinite due to eliminating way too many points
                calc_para_o();
            }


            //from the new parameters doing a maximum log minimization with minuit
            TMinuit *gMinuit3 = new TMinuit(2);
            TMinuit *gMinuit4 = new TMinuit(2);
            gMinuit3->SetFCN(fcn2);
            gMinuit4->SetFCN(fcn3);

            Double_t arglist2[10];
            Int_t ierflg2 = 0;


            arglist2[0] = 2;
            gMinuit3->mnexcm("SET ERR", arglist2 ,1,ierflg2);
            gMinuit4->mnexcm("SET ERR", arglist2 ,1,ierflg2);



            static Double_t step2[4] = {0.001 , 0.001 , 0.001 , 0.001};
            gMinuit3->mnparm(0, "aa1", p1, step2[0], 0,0,ierflg2);
            gMinuit3->mnparm(1, "aa2", p2, step2[1], 0,0,ierflg2);

            gMinuit4->mnparm(0, "aa3", p3, step2[2], 0,0,ierflg2);
            gMinuit4->mnparm(1, "aa4", p4, step2[3], 0,0,ierflg2);

            arglist2[0] = 500;
            arglist2[1] = 1.;

            gMinuit3->mnexcm("MIGRAD", arglist2 ,2,ierflg2);

            gMinuit4->mnexcm("MIGRAD", arglist2 ,2,ierflg2);


            gMinuit3->GetParameter(0,p1,erp1);
            gMinuit3->GetParameter(1,p2,erp2);
            gMinuit4->GetParameter(0,p3,erp3);
            gMinuit4->GetParameter(1,p4,erp4);



            vector<double> ox, oy, oz;
            double dt =0;

            while(dt<2){
                ox.push_back(o_xpos);
                oy.push_back(o_ypos);
                oz.push_back(o_zpos);
                dt += 0.9;
                o_xpos = o_xpos + (2*o_xsp);
                o_ypos = o_ypos + (2*o_ysp);
                o_zpos = o_zpos + (2*o_zsp);
            }

            Double_t *OOX = new Double_t[ox.size()];
            Double_t *OOY = new Double_t[oy.size()];
            Double_t *OOZ = new Double_t[oz.size()];

            for (int i=0;i<ox.size();i++){
                OOX[i] = ox[i];
                OOY[i] = oy[i];
                OOZ[i] = oz[i];
            }

            Int_t NPO = ox.size();



            zpos = 30150;
            xpos = (zpos*p2)+p1;
            ypos = (zpos*p4)+p3;
            long double pp1,pp2,pp3,pp4;
            pp1=p1;
            pp2=p2;
            pp3=p3;
            pp4=p4;
            res <<"     " << NN << "                     " << p1 << " " << p2 << " " << p3 << " " << p4 << "                " << xpos << " " << ypos << " " << zpos << endl;
            long double Fname = NN;

            Int_t NPL1 = x.size();
            Int_t NPL2 = y.size();
            Double_t *XPL = new Double_t[NPL1];
            Double_t *YPL = new Double_t[NPL2];
            Double_t *ZXPL = new Double_t[NPL1];
            Double_t *ZYPL = new Double_t[NPL2];
            Double_t *XSI = new Double_t[NPL1];
            Double_t *YSI = new Double_t[NPL2];
            Double_t *ZXSI = new Double_t[NPL1];
            Double_t *ZYSI = new Double_t[NPL2];

            //changing vectors to arrays to use them in TGraf
            for (int s = 0; s< NPL1 ; s++){
                XPL[s] = x[s];
                ZXPL[s] = xz[s];
                XSI[s] = xsig[s];
                ZXSI[s] = xzsig[s];

            }
            for (int s = 0; s< NPL2 ; s++){
                YPL[s] = y[s];
                ZYPL[s] = yz[s];
                YSI[s] = ysig[s];
                ZYSI[s] = yzsig[s];
            }





            TGraph *gr1 = new TGraphErrors(NPL1,ZXPL,XPL,ZXSI,XSI);
            TGraph *o_gr1 = new TGraph(NPO,OOZ,OOX);
            gr1->SetTitle("The fit in the x-axis");
            gr1->GetHistogram()->GetYaxis()->SetTitle("X-position");
            gr1->GetHistogram()->GetXaxis()->SetTitle("Z-position");
            TCanvas *c1 = new TCanvas("c1","my2dplots",1980,1080);
            string namef1 = to_string(pp1); //to_string in VS 2010only accept 3 types of variables one of them long double make sure to use that
            namef1.append("+(");
            namef1 +=to_string(pp2);
            namef1 +="*x)";
            TF1 *f1 = new TF1("f1",namef1.c_str(),gr1->GetXaxis()->GetXmin(),gr1->GetXaxis()->GetXmax());

            gr1->Draw("AP");
            o_gr1->Draw("SAME");
            f1 ->Draw("SAME");

            for(int i=0; i< x_t.size();i++){ // marking the selected points
                TMarker *m1 = new TMarker(xz_t[i],x_t[i],5);
                m1 ->SetMarkerSize(7);
                m1 ->SetMarkerColor(4);
                m1 ->Draw("SAME");

            }


            string filename1 = to_string(Fname);
            filename1 += "x.png";
            c1->SaveAs(filename1.c_str());


            TGraph *gr2 = new TGraphErrors(NPL2,ZYPL,YPL,ZYSI,YSI);
            TGraph *o_gr2 = new TGraph(NPO,OOZ,OOY);
            gr2->SetTitle("The fit in the y-axis");
            gr2->GetHistogram()->GetYaxis()->SetTitle("Y-position");
            gr2->GetHistogram()->GetXaxis()->SetTitle("Z-position");
            TCanvas *c2 = new TCanvas("c2","my2dplots",1980,1080);
            string namef2 = to_string(pp3);
            namef2.append("+(");
            namef2 +=to_string(pp4);
            namef2 +="*x)";
            TF1 *f2 = new TF1("f2",namef2.c_str(),gr2->GetXaxis()->GetXmin(),gr2->GetXaxis()->GetXmax());
            gr2->Draw("AP");
            o_gr2->Draw("SAME");
            f2 ->Draw("SAME");

            for(int i=0; i< y_t.size();i++){
                TMarker *m2 = new TMarker(yz_t[i],y_t[i],5);
                m2 ->SetMarkerSize(7);
                m2 ->SetMarkerColor(4);
                m2 ->Draw("SAME");

            }


            string filename2 = to_string(Fname);
            filename2 += "y.png";
            c2->SaveAs(filename2.c_str());


            delete[] XPL;
            delete[] ZXPL;
            delete[] XSI;
            delete[] ZXSI;
            delete[] YPL;
            delete[] ZYPL;
            delete[] YSI;
            delete[] ZYSI;
            delete[] OOX;
            delete[] OOY;
            delete[] OOZ;

            double o_dx,o_dy,o_dz,posx2,posy2,posz2,dist,lent,dtheta;

            o_dx = ox.front()-ox.back();
            o_dy = oy.front()-oy.back();
            o_dz = oz.front()-oz.back();

            posz2 = oz.back();
            posx2 = (posz2*p2)+p1;
            posy2 = (posz2*p4)+p3;

            dist = sqrt(pow(ox.back()-posx2,2)+pow(oy.back()-posy2,2)+pow(oz.back()-posz2,2));
            lent = sqrt(pow(o_dx,2)+pow(o_dy,2)+pow(o_dz,2));



            dtheta = atan(dist/lent)*180/M_PI; //finding the opening angles


            if(dtheta!=dtheta){;}
            else{

                thetavalues.push_back(dtheta);
            }

            pointsx << NN << endl;
            pointsy << NN << endl;


            for (int i =0 ;i < x_t.size() ; i++){
                pointsx << x_t[i] << endl;
            }
            for (int i =0 ;i < y_t.size() ; i++){
                pointsy << y_t[i] << endl;
            }
            pointsx <<  endl;
            pointsy <<  endl;
            pointsx <<  endl;
            pointsy <<  endl;


            theta << NN << " "  << dtheta << endl;


            NN++;
        }
    }
    TCanvas *cx = new TCanvas("cx","my2dplots",1980,1080);
    TH1F *hxt = new TH1F("hxt","myhist2", int(*max_element(TXtot.begin(),TXtot.end()) - *min_element(TXtot.begin(),TXtot.end())) , *min_element(TXtot.begin(),TXtot.end()) - 3 , *max_element(TXtot.begin(),TXtot.end()) + 3  );
    for(int i =0; i<TXtot.size();i++) {hxt->Fill(TXtot[i]);}
    hxt -> Draw();
    cx -> SaveAs("totalx.png");



    TCanvas *cy = new TCanvas("cy","my2dplots",1980,1080);
    TH1F *hyt = new TH1F("hyt","myhist2", int(*max_element(TYtot.begin(),TYtot.end()) - *min_element(TYtot.begin(),TYtot.end())) , *min_element(TYtot.begin(),TYtot.end()) - 3 , *max_element(TYtot.begin(),TYtot.end()) + 3  );
    for(int i =0; i<TYtot.size();i++) {hyt->Fill(TYtot[i]);}
    hyt -> Draw();
    cy -> SaveAs("totaly.png");



}
int main() {
   fitting();

   ofstream thm ("median.txt");
   ofstream iii ("something.txt");
   sort(thetavalues.begin(),thetavalues.end());
   double theta_median = thetavalues[thetavalues.size()/2]; //finding the resolution form the opening angles


   for (int j =0; j< thetavalues.size(); j++){
    iii<< thetavalues[j] << endl;
   }

   thm << theta_median << endl;

   return 0;
}
