# Consistent Contraints method for numerical analytical continuation
Trying to find $`A(z)`$ from
```math
g_n = G[n,A] = \int_{-\infty}^{+\infty} K(n,z) A(z) dz
```
Where $$K(n,z)$$ is a kernel, and $$g_n$$ a set of numerical or experimental points with error bars. This problem is difficult to solve numerically because of the ambiguaty of the solution due to the error bars. This library uses work done in [^fn1] [^fn2] [^fn3] to find so called basic solutions from a set of configurations and then uses the Consisntent Constraints method to smooth the final solution.

# Theory
As mentioned above one of the reseaons that it is hard to find $A(z)$ is the ambiguity of the solution as there are an infinite number of solutions that solve 

\begin{equation}
A+B =C
\end{equation}

test

# Tutorial


## References
[^fn1]:Nikolay Prokof’ev and Boris Svistunov. “Spectral Analysis by the Method of Consis-tent Constraints”. en. In:JETP Letters97.11 (Aug. 2013). arXiv: 1304.5198, pp. 649–653.issn: 0021-3640, 1090-6487.doi:10.1134/S002136401311009X.url:http://arxiv.org/abs/1304.5198

[^fn2]:Olga Goulko et al. “Numerical analytic continuation: Answers to well-posed ques-tions”. en. In:Physical Review B95.1 (Jan. 2017). arXiv: 1609.01260, p. 014102.issn:2469-9950, 2469-9969.doi:10.1103/PhysRevB.95.014102.url:http://arxiv.org/abs/1609.01260

[^fn3]:A. Mishchenko et al. “Diagrammatic quantum Monte Carlo study of the Fröhlichpolaron”. en. In:Physical Review B62.10 (Sept. 2000), pp. 6317–6336.issn: 0163-1829, 1095-3795.doi:10.1103/PhysRevB.62.6317.url:https://link.aps.org/doi/10.1103/PhysRevB.62.6317